package com.itau.jogovelha.requester;

import com.github.kevinsawicki.http.HttpRequest;

public class ApiRequester {

    public static String get(String baseUrl) {
        return HttpRequest.get(baseUrl).header("Content-type", "application/json").body();
    }
    public static String post(String baseUrl, String json) {
        return HttpRequest
                .post(baseUrl)
                .header("Content-type", "application/json")
                .send(json)
                .body();
    }
}
