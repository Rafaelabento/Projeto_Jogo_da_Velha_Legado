package com.itau.jogovelha.model.externo;

public class Jogador {
    public String nome;
    public int pontos;

    public Jogador(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public int getPontos() {
        return pontos;
    }

    public void incrementarPontos(){
        this.pontos++;
    }
}
