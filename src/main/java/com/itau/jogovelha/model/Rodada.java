package com.itau.jogovelha.model;

import com.google.gson.Gson;
import com.itau.jogovelha.model.externo.Placar;
import com.itau.jogovelha.mq.Evento;
import com.itau.jogovelha.requester.ApiRequester;

public class Rodada {
    private Jogo jogo;

    public Rodada(String nomeUm, String nomeDois){
        Evento.send("g4.monolito.Rodada.Rodada");
        String jogadorUm = String.format("{\"nome\":\"%s\"}", nomeUm);
        String jogadorDois = String.format("{\"nome\":\"%s\"}", nomeDois);
        ApiRequester.post("http://localhost:8082/jogador",jogadorUm);
        ApiRequester.post("http://localhost:8082/jogador",jogadorDois);
        jogo = new Jogo();
    }

    public void iniciarJogo(){
        Evento.send("g4.monolito.Rodada.iniciarJogo");

        if(! jogo.isEncerrado()){
            return;
        }

        jogo = new Jogo();
    }

    public void jogar(int x, int y){
        Evento.send("g4.monolito.Rodada.jogar");

        if(jogo.isEncerrado()){
            return;
        }

        jogo.jogar(x, y);

        if(jogo.isEncerrado() && jogo.isVitoria()){
            int vencedor = jogo.getJogadorAtivo();
            ApiRequester.post("http://localhost:8082/jogador/pontos/incrementar",Integer.toString(vencedor));
        }
    }

    public Placar getPlacar() {
        Evento.send("g4.monolito.Rodada.getPlacar");

        String response = ApiRequester.get("http://localhost:8081/placar");
        Placar placar = new Gson().fromJson(response, Placar.class);
        placar.encerrado = jogo.isEncerrado();
        placar.casas = jogo.getCasas();
        return  placar;
    }

}
